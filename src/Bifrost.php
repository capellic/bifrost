<?php

namespace Capellic\Bifrost;

use Capellic\Bifrost\Commands\DeployCommand;
use Capellic\Bifrost\Commands\InitCommand;
use Consolidation\Config\ConfigInterface;
use Consolidation\Config\Loader\YamlConfigLoader;
use League\Container\Container;
use League\Container\ContainerAwareInterface;
use League\Container\ContainerAwareTrait;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Robo\Common\ConfigAwareTrait;
use Robo\Config\Config;
use Robo\Contract\ConfigAwareInterface;
use Robo\Robo;
use Robo\Runner;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Bifrost implements ConfigAwareInterface, ContainerAwareInterface, LoggerAwareInterface
{
    use ConfigAwareTrait;
    use ContainerAwareTrait;
    use LoggerAwareTrait;

    public function __construct(Config $config, InputInterface $input = null, OutputInterface  $output = null)
    {
        $this->setConfig($config);
        $application = new Application('Bifrost', $config->get('version'));
        // Use deprecated command for compatibility with Terminus/Drush usage.
        $container = Robo::createDefaultContainer($input, $output, $application, $config);
        $this->setContainer($container);
        $this->configureContainer();
        $this->commands = [
        InitCommand::class,
        DeployCommand::class
        ];
        $this->setLogger($container->get('logger'));
        $this->runner = new Runner();
        $this->runner->setContainer($container);
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        return $this->runner->run($input, $output, null, $this->commands);
    }

    private function configureContainer()
    {
    }
}
