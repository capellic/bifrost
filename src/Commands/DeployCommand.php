<?php

namespace Capellic\Bifrost\Commands;

use Drush\Drush;
use JoliCode\Slack\ClientFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class DeployCommand extends BifrostCommand
{

  /**
   * This is the deploy command
   *
   * This comamnd will run standard post-code-deploy steps on Pantheon and collect
   * the output to send to Slack to ensure the team is notified of deploy results.
   *
   * @command deploy
   * @aliases d
   * @usage deploy
   */
    public function deploy()
    {
        $pantheonSite = getenv('PANTHEON_SITE');
        $pantheonEnv = getenv('PANTHEON_ENVIRONMENT');
        $client = ClientFactory::create($config->get('slack_token'));
        $client->chatPostMessage([
            'username' => 'Bifrost',
            'channel' => $this->config->get('slack_channel'),
            'text' => "Running deploy hooks on ${pantheonSite} ${pantheonEnv} (not really, Dustin is faking it)..."
        ]);
        $process = new Process('drush deploy --no-interaction --no-ansi -y 2>&1');
        $process->setIdleTimeout(60);
        $process->run();
        $this->io()->write($process->getOutput());
        $this->postDeploySummary($client, $process);
    }

  /**
   * @param \JoliCode\Slack\Client $client
   * @param \Symfony\Component\Process\Process $process
   */
    protected function postDeploySummary(\JoliCode\Slack\Client $client, Process $process)
    {
        $client->chatPostMessage([
        'username' => 'Bifrost',
        'channel' => $this->config->get('slack_channel'),
        'text' => $process->getOutput(),
        'blocks' => json_encode([
        [
          'type' => 'header',
          'text' => [
            'type' => 'plain_text',
            'text' => 'Deploy summary:',
          ]
        ],
        [
          'type' => 'section',
          'text' => [
            'type' => 'plain_text',
            'text' => $process->getOutput(),
          ],
        ],
        ]),
        ]);
    }
}
