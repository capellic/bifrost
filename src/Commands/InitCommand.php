<?php

namespace Capellic\Bifrost\Commands;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class InitCommand extends BifrostCommand
{


  /**
   * @command init
   */
    public function init()
    {
        $filesystem = new Filesystem();
        try {
            $this->createEntrypoint($filesystem);
            $this->createConfigFile($filesystem);
        } catch (\Exception $exception) {
            $output->write($exception->getMessage());
        }
    }

  /**
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   */
    protected function createEntrypoint(Filesystem $filesystem)
    {
        $targetScript = getcwd() . '/web/private/scripts/bifrost.php';
        $stubScript = dirname(__DIR__, 2) . '/stubs/bifrost.php';
        $filesystem->copy($stubScript, $targetScript);
        $filesystem->chmod($targetScript, 1000);
    }

  /**
   * @param \Symfony\Component\Filesystem\Filesystem $filesystem
   */
    protected function createConfigFile(Filesystem $filesystem)
    {
        $defaultConfig = ['slack_token' => '', 'slack_channel' => ''];
        $filesystem->touch(getcwd() . '/bifrost.yml');
        $filesystem->appendToFile(getcwd() . '/bifrost.yml', Yaml::dump($defaultConfig));
    }
}
