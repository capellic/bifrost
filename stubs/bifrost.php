#!/usr/bin/env php
<?php
// application.php
// this script should be running from /web/private/scripts
require dirname(__DIR__, 3) . '/vendor/autoload.php';

use Capellic\Bifrost\Bifrost;
use Capellic\Bifrost\Commands\DeployCommand;
use Consolidation\Config\Loader\YamlConfigLoader;
use Symfony\Component\Config\FileLocator;

$application = new Bifrost('Bifrost', '0.0.1');

$configDirectories = array(getcwd());
$locator = new FileLocator($configDirectories);
$loader = new YamlConfigLoader($locator);

$config  = $loader->load(file_get_contents($locator->locate("bifrost.yml")));
dump($config);
$application->add(new DeployCommand());

$application->run();
